# CSV Split

Developed to split large customer-events.csv files up into individual files of 1,000,000 rows, then GZipped to reduce size.
Primarily designed to save some money by processing the splitting on a local machine instead of having the Data Lake process it at costly prices!

# Building

Developed using Visual Studio Community 2017, no extensions were used.
Simply clone and build solution from the build menu (or use F5 to build and run).

The built files can be found in `\CSVSplitConsole\bin\debug\CSVSplitConsole.exe`
