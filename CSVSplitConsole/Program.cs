﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;

namespace CSVSplitConsole
{
    class Program
    {
        /// <summary>
        /// Variable definitions
        /// </summary>
        private static double currPos = 0;
        private static double remItems = 1000000;
        private static double objSize = 1000000;

        private static int currPart = 1;
        private static long elapsedMs = 0;

        private static Stopwatch watch = Stopwatch.StartNew();
       
        /// <summary>
        /// Entrypoint for the application
        /// </summary>
        /// <param name="args">Contains arguments, mainly the file to process</param>
        static void Main(string[] args)
        {
            try
            {
                // Open and read file passed in via argument parameters
                using (var reader = new StreamReader(args[0]))
                {
                    // Read the very first line of the header, this will be placed in every part file
                    string headers = reader.ReadLine();

                    // Create a new part file using the current part that we're processing
                    var stream = File.CreateText("customer-events-" + currPart.ToString() + ".csv");

                    // Start processing and read until end of file
                    while (!reader.EndOfStream)
                    {
                        // If we're writing the first line of this part
                        if (currPos == 0)
                        {
                            // Set the headers first
                            stream.WriteLine(headers);
                        }

                        // Read from base file and write into part file
                        stream.WriteLine(reader.ReadLine());

                        // Console output
                        Console.Write("\rWrote line {0} of {1} | {2}% | Elapsed: {3} seconds", currPos, objSize, Math.Round((currPos / objSize) * 100), watch.ElapsedMilliseconds / 1000);
    
                        // Increment the current position for this part
                        currPos++;

                        // Check if we're at the maximum number of rows per-file
                        if (currPos == objSize)
                        {
                            // Close this stream part file
                            stream.Close();

                            // Finalise the operation for this part, handles compression and release of files.
                            Finalise();

                            // Reset stopwatch
                            watch.Stop();
                            watch = Stopwatch.StartNew();
                            
                            // Reset position
                            currPos = 0;

                            // Make the next part
                            stream = File.CreateText("customer-events-" + currPart.ToString() + ".csv");
                        }
                    }

                    // In the case we hit end of file and the current position is more than 0 (meaning we've gone X lines in and now finalised it)
                    if(currPos > 0)
                    {
                        Finalise();

                        Console.WriteLine("Done!\nPress enter to exit.");
                        Console.ReadLine();
                        Environment.Exit(0);
                    }
                }
            }
            catch (Exception ex)
            {
                // This exeption will usually occur on no input file sent in from argument parameters
                Console.WriteLine("Exception caught, this usually happens when no file is specified.?\nException: {0}\nPress enter to exit.", ex.Message);
                Console.ReadLine();
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Handles compression of the CSV file processed
        /// </summary>
        private static void Finalise()
        {
            // Console output the final results
            Console.Write("\rWrote line {0} of {1} | {2}% | Finished: {3} seconds", currPos, objSize, Math.Round((currPos / objSize) * 100), watch.ElapsedMilliseconds / 1000);

            // Stop the timer and reset
            watch.Stop();
            watch = Stopwatch.StartNew();

            // Compress the output CSV file
            FileInfo compressFile = new FileInfo("customer-events-" + currPart.ToString() + ".csv");
            using (MemoryStream compressedMemStream = new MemoryStream())
            {
                using (FileStream originalFileStream = compressFile.OpenRead())
                using (GZipStream compressionStream = new GZipStream(compressedMemStream, CompressionMode.Compress, leaveOpen: true))
                {
                    originalFileStream.CopyTo(compressionStream);
                    originalFileStream.Close();
                }
                compressedMemStream.Seek(0, SeekOrigin.Begin);

                FileStream compressedFileStream = File.Create(compressFile.FullName + ".gz");
                compressedMemStream.WriteTo(compressedFileStream);
                compressedFileStream.Close();
            }

            // Debug output
            Console.WriteLine("\nGZip Done: {0} seconds", watch.ElapsedMilliseconds / 1000);
            watch.Stop();

            // Setup next part
            currPart++;
        }
    }
}

